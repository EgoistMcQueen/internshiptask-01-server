const express = require("express");
const router = express.Router();
const db = require("../models")

const findConnectionArray = (users, result, name, secondPerson) => {
    console.log(name)
    let foundUser = users.find((user) =>{
        return user.name == name
    })
    if(!foundUser) return result;
    result.push(name);
    for(let relation of foundUser.relations){
        console.log(result)
        if(relation.relatedTo == secondPerson){
            result.push(relation.relatedTo)
            return result
        }
        else result = findConnectionArray(users, result, relation.relatedTo, secondPerson)
        if(result[result.length-1]==secondPerson) break;
    }
    return result;
}

router.post("/newRelation", async (req, res) => {
    console.log(`${req.body.name} is ${req.body.relationType} of ${req.body.relatedTo}`)
    let {relationType, relatedTo} = req.body;
    const relation = await db.Relation.create({relationType, relatedTo});
    let user;
    user = await db.User.findOne({name: req.body.name})
    if(!user){
        user = await db.User.create({name: req.body.name})
    }
    user.relations.push(relation._id)
    await user.save()
    let relations = await db.User.find()
    return res.json({relations})
})

router.get("/getAllRelations", async (req, res) => {
    let relations = await db.User.find()
    return res.json({relations})
})

router.post("/findConnection", async (req, res) => {
    let users = await db.User.find().populate("relations")
    let result = findConnectionArray(users, [], req.body.firstPerson, req.body.secondPerson)
    if(result.length) return res.json({result})
    return res.json({result: ["Not found!"]})
})
module.exports = router;