const mongoose = require("mongoose")

const RelationSchema = new mongoose.Schema({
    relationType: {
        type: String
    },
    relatedTo: {
        type: String
    }
})

module.exports = Relation = mongoose.model("Relation", RelationSchema);