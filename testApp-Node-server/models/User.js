const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    relations: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Relation",
        autopopulate: true
    }]
})
UserSchema.plugin(require("mongoose-autopopulate"))

module.exports = User = mongoose.model("users", UserSchema);