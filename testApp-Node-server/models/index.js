const mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);
mongoose.Promise = Promise;
mongoose.connect("mongodb://127.0.0.1:27017/testApplication", {
    keepAlive: true, 
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(()=>{
    console.log("MongoDB connected!")
}).catch((err)=>{
    console.log("Database connection timed out!")
})

module.exports.User = require("./User")
module.exports.Relation = require("./Relation.js")