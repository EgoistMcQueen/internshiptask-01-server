const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const Routes = require("./routes")

app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
const db  = require("./models");
app.use(bodyParser.json());
app.use("/api", Routes)

const port = process.env.PORT || 5000; 
app.listen(port, () => console.log(`Server up and running on port ${port} !`));