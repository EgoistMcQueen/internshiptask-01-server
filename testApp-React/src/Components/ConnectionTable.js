const ConnectionTable = ({people}) => {
    let tableRows;
    if(people.length) {
        tableRows = people.map((person) => {
            return person.relations.map((relation, i)=>{
                return (
                    <tr key={i}>
                        <th scope="row">{"=>"}</th>
                        <td>{person.name} is {relation.relationType} of {relation.relatedTo}.</td>
                    </tr>
                )
            })
        })
    }

    return (
        <div className="table-responsive">
            <table className="table">
                <caption>List of users</caption>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Connection</th>
                    </tr>
                </thead>
                <tbody>
                    {tableRows}
                </tbody>
            </table>
        </div>
    )
}
export default ConnectionTable;