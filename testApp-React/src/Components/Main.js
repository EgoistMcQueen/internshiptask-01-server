import ConnectionTable from './ConnectionTable';
import React, {Component} from "react"
import NewConnectionForm from './NewConnectionForm';
import FindConnectionForm from './FindConnectionForm';
import axios from "axios"
import SpinnerPage from './SpinnerPage';

class Main extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: "",
            foundMessage: [],
            people: {},
            findLoadState: false,
            addLoadState: false
        }
    }

    componentDidMount(){
        this.setState({addLoadState: !this.state.addLoadState})
        axios.get("http://localhost:5000/api/getAllRelations")
        .then((res) => {
            this.setState({people: res.data.relations, addLoadState: !this.state.addLoadState})
        })
        .catch((err)=> {
            alert(err)
        })
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmitNew = (e) => {
        e.preventDefault()
        this.setState({addLoadState: !this.state.addLoadState})
        let data = {...this.state}
        axios.post("http://localhost:5000/api/newRelation", data)
        .then((res) => {
            this.setState({people: res.data.relations, addLoadState: !this.state.addLoadState})
        })
        .catch((err)=> {
            alert(err)
        })
        e.target.reset()
       
    }

    onClose = (e) => {
        this.setState({foundMessage: [], firstPerson: null, secondPerson: null})
    }

    onSubmitFind = (e) => {
        e.preventDefault()
        this.setState({findLoadState: !this.state.findLoadState})
        let data = {...this.state}
        axios.post("http://localhost:5000/api/findConnection", data)
        .then((res)=>{
            this.setState({foundMessage: res.data.result, findLoadState: !this.state.findLoadState})
        })
        e.target.reset()
    }

    render(){
        return (
            <div className="container">
                <NewConnectionForm onSubmitNew={this.onSubmitNew} onChange={this.onChange}></NewConnectionForm>
                <br></br>
                {this.state.addLoadState && <SpinnerPage></SpinnerPage>}
                <br></br>
                <ConnectionTable people={this.state.people}></ConnectionTable>
                <br></br>
                <FindConnectionForm onClose={this.onClose} onSubmitFind={this.onSubmitFind} onChange={this.onChange} foundMessage={this.state.foundMessage}></FindConnectionForm>
            </div>
        )
    }
}
export default Main;