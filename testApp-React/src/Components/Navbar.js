import {NavLink} from "react-router-dom"

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <NavLink className="navbar-brand" to="/">Visualization</NavLink>
            <ul className="navbar-nav ml-auto mt-lg-0">
                <li className="nav-item">
                    <NavLink activeClassName="active" exact className="nav-link" to="/findConnections" data-toggle="modal" data-target="#exampleModal">
                        Find Connection!
                    </NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar;