const FindConnectionForm = ({onSubmitFind, onChange, foundMessage, onClose}) => {

    let connection;
    if(foundMessage.length > 1){
        connection = foundMessage.map((person)=>{
            return " => " + person
        })
    }
    else if(foundMessage.length) connection = "Not found!"
    return (
        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Find Connection!</h5>
                        <button onClick={onClose} type="reset" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={onSubmitFind}>
                            <div className="form-row align-items-center">
                                <div className="col-auto">
                                    <label className="sr-only" htmlFor="name">First Person</label>
                                    <input onChange={onChange} type="text" className="form-control mb-2" id="name" name="firstPerson" placeholder="First Person"></input>
                                </div>
                                <div className="col-auto">
                                    <label className="sr-only" htmlFor="relatedTo">Second Person</label>
                                    <input onChange={onChange} type="text" className="form-control mb-2" id="relatedTo" name="secondPerson" placeholder="Second Person"></input>
                                </div>
                                <div className="col-auto">
                                    <button onChange={onChange} type="submit" className="btn btn-primary mb-2">Find!</button>
                                </div>
                                <div className="col-auto">
                                    <button onClick={onClose} type="reset" className="btn btn-secondary mb-2" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                        {connection && (<div>
                            <hr></hr>
                            {connection}
                            <hr></hr>
                        </div>)}
                    </div>
                </div>
            </div>
        </div>
       
    )
}

export default FindConnectionForm;