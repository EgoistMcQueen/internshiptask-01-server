import React from "react";

const SpinnerPage = () => {
  return (
    <>
      <div className="spinner-border mb-5" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </>
  );
}

export default SpinnerPage;