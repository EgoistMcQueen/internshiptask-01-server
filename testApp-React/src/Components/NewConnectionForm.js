const NewConnectionForm = ({onSubmitNew, onChange}) => {
    return (
        <form onSubmit={onSubmitNew}>
            <div className="form-row align-items-center">
                <div className="col-auto">
                    <label className="sr-only" htmlFor="name">First Person</label>
                    <input onChange={onChange} type="text" className="form-control mb-2" id="name" name="name" placeholder="First Person"></input>
                </div>
                <div className="col-auto">
                    <label className="sr-only" htmlFor="relatedTo">Second Person</label>
                    <input onChange={onChange} type="text" className="form-control mb-2" id="relatedTo" name="relatedTo" placeholder="Second Person"></input>
                </div>
                <div className="col-auto">
                    <label className="sr-only" htmlFor="relationType">Connection Type</label>
                    <input onChange={onChange} type="text" className="form-control mb-2" id="relationType" name="relationType" placeholder="Connection Type"></input>
                </div>
                <br></br>
                <div className="col-auto">
                    <button onChange={onChange} type="submit" className="btn btn-primary mb-2">Add Connection!</button>
                </div>
            </div>
        </form>
    )
}

export default NewConnectionForm;