import Navbar from "./Components/Navbar"
import { BrowserRouter as Router } from "react-router-dom"
import Main from "./Components/Main"

function App() {
  return (
      <Router>
        <div className="body-main">
          <Navbar></Navbar>
          <br></br>
          <Main></Main>
        </div>
      </Router>
  );
}

export default App;
